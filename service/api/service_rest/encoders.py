from common.json import ModelEncoder
from .models import Technician, Appointment, AutomobileVO

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]

class AppointmentsEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "vin",
        "technician",
        "customer",
    ]
    encoders= {
        "technician": TechnicianEncoder(),
    }
    def get_extra_data(self, o):
        return {"status": o.status.name}
