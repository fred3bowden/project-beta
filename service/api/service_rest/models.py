from django.db import models

# Create your models here.
class Technician(models.Model):
    first_name=models.CharField(max_length=200)
    last_name=models.CharField(max_length=200)
    employee_id=models.CharField(max_length=200)


class Status(models.Model):
    id = models.PositiveSmallIntegerField(primary_key=True)
    name = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("id",)  # Default ordering for Status
        verbose_name_plural = "statuses"

class Appointment(models.Model):

    @classmethod
    def create(cls, **kwargs):
        kwargs["status"] = Status.objects.get(name="created")
        appointment = cls(**kwargs)
        appointment.save()
        return appointment

    date_time=models.DateTimeField()
    reason=models.CharField(max_length=20000)
    vin=models.CharField(max_length=200)
    customer=models.CharField(max_length=200, null=True)
    status=models.ForeignKey(
        Status,
        related_name="appointment",
        on_delete=models.PROTECT,
    )

    technician=models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.PROTECT,
    )

    def finish(self):
        status = Status.objects.get(name="finished")
        self.status = status
        self.save()

    def cancel(self):
        status = Status.objects.get(name="canceled")
        self.status = status
        self.save()


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)
