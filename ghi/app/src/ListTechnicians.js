import { useEffect, useState } from "react";

function ListTechnicians() {
    const [technicians, setTechnicians] = useState([])
    const fetchData = async () => {
        const response = await fetch('http://localhost:8080/api/technicians/');
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
  } else {
    console.error(response);
  }}
  useEffect(() => {
    fetchData();
  }, []);

  return(
    <table className="table table-striped">
        <thead>
            <tr>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            </tr>
        </thead>
        <tbody>
            {technicians.map(tech => {
            return(<tr key={tech.id}>
                <td>{tech.employee_id}</td>
                <td>{tech.first_name}</td>
                <td>{tech.last_name}</td>
            </tr>
            )})}
        </tbody>

</table>
  )
}

export default ListTechnicians;
