import { useEffect, useState } from 'react';

function CreateModel() {
    const [name, setName] = useState('');
    const [picture_url, setPictureUrl] = useState('');
    const [manufactures, setManufacturers] = useState([]);
    const [manufacturer, setManufacturer] = useState('');

    const handleChange = (event, callback) => {
        const { target } = event;
        const { value } = target;
        callback(value);
    }

    const fetchData = async () => {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        if (response.ok) {
        const data = await response.json();
        setManufacturers(data.manufacturers);
    } else {
        console.error(response);
    }}
    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.picture_url = picture_url;
        data.manufacturer_id = manufacturer;

        const json = JSON.stringify(data);
        const url = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: 'POST', body: json, headers: {'Content-Type': 'application/json'}
        }
        const response = await fetch(url, fetchConfig);
        if(response.ok) {
            const newModel = await response.json();
            console.log(newModel);
            setName('');
            setPictureUrl('');
            setManufacturer('');
        }
    }

    return(
    <div className="justify-content-center align-items-center">
    <div className='shadow p-4 mt-4'>
        <h1>Create a vehicle model</h1>
        <form onSubmit={handleSubmit}>
        <div className="form-floating mb-3">
            <input onChange={(event) => {handleChange(event, setName)}} value={name} required placeholder="Model name..." name="name" id="name" />
        </div>
        <div className="form-floating mb-3">
            <input onChange={(event) => {handleChange(event, setPictureUrl)}} value={picture_url} required placeholder="Picture URL..." name="picture_url" id="picture_url" />
        </div>
        <div className="form-floating mb-3">
            <select onChange={(event) => {handleChange(event, setManufacturer)}} value={manufacturer} required name="manufacturer" id="manufacturer" className='form-select'>
                <option>Choose a manufacturer...</option>
                    {manufactures.map((manufacturer) => {
                        return (
                            <option key={manufacturer.id} value={manufacturer.id}>
                                {manufacturer.name}
                            </option>
                        )
                    })}
            </select>
        </div>
        <div className="form-floating mb-3">
            <button className="btn btn-primary">Create</button>
        </div>
        </form>
    </div>
    </div>
)

}

export default CreateModel;
