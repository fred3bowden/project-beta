import { useEffect, useState } from "react";

function AppointmentHistory() {
    const [appointments, setAppointments] = useState([]);
    const [autos, setAutos] = useState([]);
    const fetchAutos = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles');
    if (response.ok) {
        const data = await response.json();
        setAutos(data.autos);
    }  else {
        console.error(response);
      }
    }
    const fetchAppointments = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
  } else {
    console.error(response);
  }}
  useEffect(() => {
    fetchAppointments();
    fetchAutos();
  }, []);


function VIP(vin, autoslist) {
    let vinlist = [];

    for (const auto of autoslist){
        vinlist.push(auto.vin)}
    if (vinlist.includes(vin)){
        return <td>Yes</td>;
    } else {
        return <td>No</td>;
    }}


  return(
    <div>
        <input type="search" placeholder="Search by VIN..." />
        <button>Search</button>

        <table className="table table-striped">
            <thead>
                <tr>
                <th>VIN</th>
                <th>Is VIP?</th>
                <th>Customer</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
                <th>Status</th>
                </tr>
            </thead>
            <tbody>
                {appointments.map(appt => {
                return(<tr key={appt.id}>
                    <td>{appt.vin}</td>
                        {VIP(appt.vin, autos)}
                    <td>{appt.customer}</td>
                    <td>{appt.date_time}</td>
                    <td>time</td>
                    <td>{appt.technician.first_name} {appt.technician.last_name}</td>
                    <td>{appt.reason}</td>
                    <td>{appt.status}</td>
                </tr>
                )})}
            </tbody>
        </table>
    </div>
  )
            }

export default AppointmentHistory;
