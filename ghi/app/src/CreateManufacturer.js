import { useEffect, useState } from "react";

function CreateManufacturer() {
    const [name, setName] = useState('');

    const handleNameChange = (event) =>{
        const value = event.target.value;
        setName(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name=name;
        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/'
        const fetchOptions = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const manufacturerResponse = await fetch(manufacturerUrl, fetchOptions);
        if (manufacturerResponse.ok) {
            setName('');
            const newManufacturer = await manufacturerResponse.json();
            console.log(newManufacturer);
        }
    }
    return(
        <div className="row">
            <div className='shadow p-4 mt-4'>
                <h1>Create a manufacturer</h1>
                <form onSubmit={handleSubmit}>
                <div className="form-floating mb-3">
                    <input onChange={handleNameChange} required placeholder="Enter name here..." name="name" id="name" />
                </div>
                <div className="form-floating mb-3">
                    <button className="btn btn-primary">Create</button>
                </div>
                </form>
            </div>
        </div>

    )
}

export default CreateManufacturer;
