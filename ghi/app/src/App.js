import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ListManufacturer from './ListManufacturers';
import CreateManufacturer from './CreateManufacturer';
import ListAutomobiles from './ListAutomobiles';
import CreateAutomobile from './CreateAutomobile';
import ListModels from './ListModels';
import CreateModel from './CreateModel';
import ListSalesPerson from './ListSalesPerson';
import CreateSalesPerson from './CreateSalesPerson';
import ListCustomers from './ListCustomers';
import CreateCustomer from './CreateCustomer';
import ListSales from './ListSales';
import CreateSale from './CreateSale';
import SalesPersonHistory from './SalesPersonHistory';
import ListTechnicians from './ListTechnicians';
import ListAppointments from './ListAppointments';
import CreateTechnician from './CreateTechnician';
import CreateAppointment from './CreateAppointment';
import AppointmentHistory from './AppointmentHistory';

function App() {

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/manufacturers/" element={<ListManufacturer />} />
          <Route path="/manufacturers/create" element={<CreateManufacturer />} />
          <Route path="/automobiles/" element={<ListAutomobiles />} />
          <Route path='/automobiles/create/' element={<CreateAutomobile/>} />
          <Route path="/models/" element={<ListModels/>} />
          <Route path="/models/create/" element={<CreateModel/>} />
          <Route path="/sales/" element={<ListSales/>} />
          <Route path="/sales/create/" element={<CreateSale />} />
          <Route path="/salespeople/" element={<ListSalesPerson/>} />
          <Route path="/salespeople/create" element={<CreateSalesPerson/>} />
          <Route path="/customers/" element={<ListCustomers/>} />
          <Route path="/customers/create/" element={<CreateCustomer/>} />
          <Route path="/sales/history/" element={<SalesPersonHistory/>} />
          <Route path="/technicians/" element={<ListTechnicians/>} />
          <Route path="/appointments/" element={<ListAppointments/>} />
          <Route path="/technicians/create/" element={<CreateTechnician />} />
          <Route path="/appointments/create/" element={<CreateAppointment />} />
          <Route path="/appointments/history/" element={<AppointmentHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
