import { useEffect, useState } from "react";

function CreateAppointment() {
    const [vin, setVin] = useState('');
    const [customer, setCustomer] = useState('');
    const [date, setDate] = useState('');
    const [technician, setTechnician] = useState('');
    const [technicians, setTechnicians] = useState([]);
    const [reason, setReason] = useState('');

    const fetchTechnicians = async () => {
        const response = await fetch('http://localhost:8080/api/technicians/');
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
  } else {
    console.error(response);
  }}
    const handleVinChange = (event) =>{
        const value = event.target.value;
        setVin(value);
    }
    const handleCustomerChange = (event) =>{
        const value = event.target.value;
        setCustomer(value);
    }
    const handleDateChange = (event) =>{
        const value = event.target.value;
        setDate(value);
    }
    const handleTechnicianChange = (event) =>{
        const value = event.target.value;
        setTechnician(value);
    }
    const handleReasonChange = (event) =>{
        const value = event.target.value;
        setReason(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.vin=vin;
        data.customer=customer;
        data.date_time=date;
        data.technician=technician;
        data.reason=reason;
        console.log(data);
        const appointmentUrl = 'http://localhost:8080/api/appointments/'
        const fetchOptions = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const appointmentResponse = await fetch(appointmentUrl, fetchOptions);
        if (appointmentResponse.ok) {
            setVin('');
            setCustomer('');
            setDate('');
            setTechnician('');
            setReason('');
            const newAppointment = await appointmentResponse.json();
            console.log(newAppointment);
        }


    }
    useEffect(() => {
        fetchTechnicians();
      }, []);
    return(
        <div>
        <h1>Make an Appointment!</h1>
            <form onSubmit={handleSubmit} id="create-appointment-form">

              <div className="form-floating mb-3">
                <input onChange={handleVinChange} value={vin} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control"/>
                <label htmlFor="vin">VIN</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCustomerChange} value={customer} placeholder="Customer" required type="text" name="customer_name" id="customer_name" className="form-control"/>
                <label htmlFor="customer_name">Customer Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleDateChange} value={date} placeholder="Schedule" required type="datetime-local" name="scheduled" id="scheduled" className="form-control"/>
                <label htmlFor="scheduled">Scheduled</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handleReasonChange} value={reason} placeholder="Reason for Service" required type="text" name="reason_for_service" id="reason_for_service" className="form-control"/>
                <label htmlFor="reason_for_service">Reason for Service</label>
              </div>

              <div className="mb-3">
                <select onChange={handleTechnicianChange} value={technician} required name="technician" id="technician" className="form-select">
                  <option value="">Choose a Technician</option>
                  {technicians.map(technician => {
                        return (
                            <option key={technician.id} value={technician.id}>
                                {technician.first_name} {technician.last_name}
                            </option>
                        );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
        </div>

    )
}

export default CreateAppointment;
